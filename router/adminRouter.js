import express from "express";
const adminRouter = express.Router();

import {
  createAdmin,
  showAdmin,
  deleteAdmin,
  loginAdmin,
} from "../controller/adminController.js";
import { refreshToken } from "../controller/refreshTokenController.js";

adminRouter.get("/", showAdmin);
adminRouter.post("/", createAdmin);
adminRouter.post("/login", loginAdmin);
adminRouter.post("/refresh", refreshToken);

adminRouter.post("/delete/:id", deleteAdmin);

export default adminRouter;
