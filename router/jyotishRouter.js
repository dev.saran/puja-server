import express from "express";
const jyotishRouter = express.Router();
import {
 
  createJyotish,
  getAllJyotish,
  getJyotishById,
  updateJyotish,
  deleteJyotish,
  
} from "../controller/jyotishController.js";






jyotishRouter.post("/", createJyotish);
jyotishRouter.get("/", getAllJyotish);
jyotishRouter.get("/:id", getJyotishById);
jyotishRouter.put("/:id", updateJyotish);
jyotishRouter.delete("/:id", deleteJyotish);


export default jyotishRouter;
