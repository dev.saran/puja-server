import express from "express";
import { createHeader, deleteHeader, getAllHeader, getHeaderById, updateHeader } from "../controller/headerController.js";
const headerRouter = express.Router();

headerRouter.post("/", createHeader);
headerRouter.get("/", getAllHeader);
headerRouter.get("/:id", getHeaderById);
headerRouter.put("/:id", updateHeader);
headerRouter.delete("/:id", deleteHeader);

export default headerRouter;
