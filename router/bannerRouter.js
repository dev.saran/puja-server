import express from "express";
const bannerRouter = express.Router();
import { createBanner, deleteBanner, getBannersData, updateBanner } from "../controller/bannerController.js";

bannerRouter.get("/", getBannersData);
bannerRouter.post("/", createBanner);
bannerRouter.delete("/:id", deleteBanner);
bannerRouter.put("/:id", updateBanner);

export default bannerRouter;
