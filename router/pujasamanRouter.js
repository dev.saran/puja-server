import express from "express";
const pujaSamanRouter = express.Router();
import {
  createPujaSaman,
  getPujaSamanbyId,
  editPujaSaman,
  updatePujaSaman,
  deletePujaSaman,
  
} from "../controller/pujasamanController.js";


pujaSamanRouter.post("/", createPujaSaman);
pujaSamanRouter.get("/", getPujaSamanbyId);
pujaSamanRouter.get("/:id", editPujaSaman);
pujaSamanRouter.put("/:id", updatePujaSaman);
pujaSamanRouter.delete("/:id", deletePujaSaman);


export default pujaSamanRouter;
