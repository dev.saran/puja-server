import express from "express";
const eventRouter = express.Router();
import { createEvent, deleteEvent, getEventData, updateEvent } from "../controller/eventController.js";

eventRouter.get("/", getEventData);
eventRouter.post("/", createEvent);
eventRouter.delete("/:id", deleteEvent);
eventRouter.put("/:id", updateEvent);

export default eventRouter;
