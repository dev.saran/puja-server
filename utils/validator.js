export const headerValidation = (title, url, status) => {
    let _error = {}
    if (!title) {
      _error['title'] = "Title is required"
    }
    else if (!url) {
        _error['url'] = "URL is required"
    }
    else if (!status) {
        _error['status'] = "Status is required"
    }  
    return _error
  };

  