import mongoose from 'mongoose';

const headerScheme = new mongoose.Schema({
  title: { type: String, required: true, trim: true },
  url: { type: String },
  status: { type: String, required: true }
}, {
  timestamps: true
});

const headerModel = mongoose.model("headers", headerScheme);

export default headerModel;