import mongoose from "mongoose";

const usersModal = new mongoose.Schema({
  name: { type: String, requred: true, trim: true },
  email: { type: String, requred: true, trim: true },
  password: { type: String, requred: true, trim: true }
}, {
  timestamps: true
});

const userModal = mongoose.model("users", usersModal);

export default userModal;
