import mongoose from 'mongoose';

const eventSchema = new mongoose.Schema({
  title: { type: String, required: true, trim: true },
  name: {type: String, required: true},
  status: { type: String, required: true },
  image: {type: String, required: true},
  description: {type: String, required: true},
}, {
  timestamps: true
});

const eventModel = mongoose.model("events", eventSchema);

export default eventModel;