import mongoose from 'mongoose';

const bannerSchema = new mongoose.Schema({
  title: { type: String, required: true, trim: true },
  type: {type: String, required: true},
  status: { type: String, required: true },
  image: {type: String, required: true}
}, {
  timestamps: true
});

const bannerModel = mongoose.model("banners", bannerSchema);

export default bannerModel;