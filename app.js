import dotenv from "dotenv";
import express from "express";
import cors from "cors";
import { join } from "path";

import router from "./router/userRouter.js";

import dbconection from "./config/databaseConfig.js";
import jyotishRouter from "./router/jyotishRouter.js";
import pujaSamanRouter from "./router/pujasamanRouter.js";
import adminRouter from "./router/adminRouter.js";
import auth from "./router/authRouter.js";
import fileUpload from "express-fileupload";
import path from 'path'
import bannerRouter from "./router/bannerRouter.js";
import authorizeMiddleware from "./middlerware/adminMiddleware.js";
import headerRouter from "./router/headerRouter.js";
import eventRouter from "./router/eventRouter.js";

const __dirname = path.resolve();
dotenv.config();

const app = express();
const port = process.env.PORT;
const database = process.env.DATABASE_URL;
const dbcon = await dbconection(database);
//file upload 
app.use(fileUpload());

//cors policy

app.use(cors());
app.use(express.json());

app.use(express.static(join(process.cwd(), "public")));

// inside public directory.
app.use("/api/images", express.static('uploads'));

app.use(express.urlencoded({ extended: true }));

app.use("/api/user", router);
app.use("/api/admin", adminRouter);
app.use("/api/jyotish", jyotishRouter);
app.use("/api/puja-saman", pujaSamanRouter);
app.use("/api", auth);
app.use("/api/banner", bannerRouter);
app.use("/api/header", headerRouter);
app.use("/api/event", eventRouter);

app.listen(port, () => {
  console.log(`sever run at http://localhost:${port}`);
});
