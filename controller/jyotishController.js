import jyotishModel from "../model/jyotishModel.js";
import path from "path";

const __dirname = path.resolve()

const createJyotish = async (req, res) => {
  const { name, email, phone, description } = req.body;

  if (name && email && phone && description) {
    try {
      let imageData = req.files.files;
      const imageName = imageData.name + new Date().getTime()
      const imageFilepath = path.join(__dirname, "/uploads/", imageName);
      imageData.mv(imageFilepath);
      const NewData = new jyotishModel({
        ...req.body,
        image: imageName
      });

      await NewData
      .save()
      .then((_data) => {
        return res.status(200).json({
          success: true,
          message: 'Data created successfully',
          data: _data,
        });
      })
      .catch((error) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Something wnet wrong. Please try again.',
          error: error.message,
        });
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        status: 400,
        message: 'Something wnet wrong. Please try again.',
        error: error.message,
      });
    }
  } else {
    res.status(402).send({
      status: 402,
      success: false,
      message: "All fields are required.",
    });
  }
};

const getAllJyotish = async (req, res) => {
  try {
    await jyotishModel.find()
      .then((_data) => {
        return res.status(200).json({
          success: true,
          sattus: 200,
          message: 'Data fetched successfully.',
          data: _data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Something went wrong. Please try again.',
          error: err.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: 'Server error. Please try again.',
      error: err.message,
    });
  }
};

const getJyotishById = async (req, res) => {
  try {
    const id = req.params.id;
    await jyotishModel.findById(id)
      .then((jyotish) => {
        res.status(200).json({
          success: true,
          message: `Data fetched successfully.`,
          data: jyotish,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Jyotish does not exist',
          error: err.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: "Server error. Please try again.",
      error: error.message,
    });
  }
};

const updateJyotish = async (req, res) => {
  try {
    const _id = req.params.id
    const updateObject = req.body;
    await jyotishModel
      .update({ _id }, { $set: req.body })
      .exec()
      .then(() => {
        res.status(200).json({
          success: true,
          message: "Successfully updated",
          updatePujaSaman: updateObject,
          status: 200,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: "Something went wrong, Please try again!",
          error: err.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: "Server error. Please try again.",
      error: error.message,
    });
  }
};

const deleteJyotish = async (req, res) => {
  try {
    const _id = req.params.id
    await jyotishModel
    .findByIdAndRemove(_id)
    .exec()
    .then(() =>
      res.status(200).send({
        status: 200,
        message: "Successfully deleted",
      })
    )
    .catch((err) =>
      res.status(400).json({
        success: false,
        status: 400,
        message: "Something went wrong, Please try again!",
        error: err.message,
      })
    );
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: "Server error. Please try again.",
      error: error.message,
    });
  }
};

export {
  createJyotish,
  getAllJyotish,
  getJyotishById,
  updateJyotish,
  deleteJyotish,
};
