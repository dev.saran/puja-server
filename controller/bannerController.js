import bannerModel from "../model/bannerModel.js";
import path from "path";

const __dirname = path.resolve();


const createBanner = async (req, res) => {
  const { title, type, status } = req.body;
  if (!req.files) {
    res.send({
      status: 402,
      message: "Banners images are required",
    });
  } else {
    let uploadedImageData = req.files.image;

    let bannerImageName = uploadedImageData.name + new Date().getTime()

    const uploadPathImage = path.join(__dirname, "/uploads/", bannerImageName);
    uploadedImageData.mv(uploadPathImage);

    try {
      const newBanner = await bannerModel.create({
        title,
        image: bannerImageName,
        type,
        status
      })

      await newBanner
      .save()
      .then((_data) => {
        return res.status(200).json({
          success: true,
          status: 200,
          message: 'Data created successfully',
          data: _data,
        });
      })
      .catch((error) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Something wnet wrong. Please try again.',
          error: error.message,
        });
      });

    } catch (error) {
      res.status(500).json({
        success: false,
        status: 500,
        message: "Server error. Please try again.",
        error: error.message,
      });
    }
  }
}
const getBannersData = async (req, res) => {
  try {
    await bannerModel.find()
      .then((_data) => {
        return res.status(200).json({
          success: true,
          sattus: 200,
          message: 'A list of all data.',
          data: _data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Something went wrong. Please try again.',
          error: err.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: 'Server error. Please try again.',
      error: err.message,
    });
  }
};

const deleteBanner = async (req, res) => {
  try {
      const _id = req.params.id
      await bannerModel
          .findByIdAndRemove(_id)
          .exec()
          .then(() =>
              res.status(200).send({
                  status: 200,
                  message: "Successfully deleted",
              })
          )
          .catch((err) =>
              res.status(400).json({
                  success: false,
                  status: 400,
                  message: "Something went wrong, Please try again!",
                  error: err.message,
              })
          );
  } catch (error) {
      res.status(500).json({
          success: false,
          status: 500,
          message: "Server error. Please try again.",
          error: error.message,
      });
  }
};

const updateBanner = async (req, res) => {
    let uploadedImageData = req.files.image;

    let bannerImageName = uploadedImageData.name + new Date().getTime()

    const uploadPathImage = path.join(__dirname, "/uploads/", bannerImageName);
    uploadedImageData.mv(uploadPathImage);
    const id = req.params.id;
    const updateObject = req.body;
    try {
      await bannerModel
      .update({ _id: id }, { $set: { ...updateObject, image: bannerImageName } })
      .exec()
      .then(() => {
        res.status(200).json({
          success: true,
          message: "Successfully updated",
          updatePujaSaman: updateObject,
          status: 200,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: "Something went wrong, Please try again!",
          error: err.message,
        });
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status: 500,
        message: "Server error. Please try again.",
        error: error.message,
      });
    }
  }



export {
  getBannersData,
  createBanner,
  deleteBanner,
  updateBanner
};
