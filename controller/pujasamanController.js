import pujaSamanModel from "../model/pujsamanModel.js";
import path from "path";

const __dirname = path.resolve();

const createPujaSaman = async (req, res) => {
  const { name, quantity, price, description, title } = req.body;
  if (!req.files.frontImage || !req.files.backImage || !req.files.thumbnail) {
    res.status(402).send({
      status: 402,
      message: "All the fields are required",
    });
  } else {
   try {
    let frontImage = req.files.frontImage;
    let backImage = req.files.backImage;
    let thumbnail = req.files.thumbnail;
    // for single image
    const frontImageFileName = frontImage.name;
    const backImageFileName = backImage.name;
    const thumbnailFileName = thumbnail.name;
    //directory to upload
    const frontImageUploadPath = path.join(__dirname, "/uploads/", frontImageFileName);
    const backImageUploadPath = path.join(__dirname, "/uploads/", backImageFileName);
    const thumbnailUploadPath = path.join(__dirname, "/uploads/", thumbnailFileName);
    frontImage.mv(frontImageUploadPath);
    backImage.mv(backImageUploadPath);
    thumbnail.mv(thumbnailUploadPath);
    if (name && quantity && price && description && title) {
      const NewPujaSaman = new pujaSamanModel({
        ...req.body,
        frontImage: frontImage.name,
        backImage: backImage.name,
        thumbnail: thumbnail.name,
      });
      await NewPujaSaman
      .save()
      .then((newSaman) => {
        return res.status(200).json({
          success: true,
          status: 200,
          message: 'Datacreated successfully',
          data: newSaman,
        });
      })
      .catch((error) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Something wnet wrong. Please try again.',
          error: error.message,
        });
      });
    } else {
      res.send({
        status: 402,
        message: "Please input all fields.",
      });
    }
  
   } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: "Server error. Please try again.",
      error: error.message,
    });
   }
   }
};

const getPujaSamanbyId = async (req, res) => {
  try {
    await pujaSamanModel.find()
      .then((allSaman) => {
        return res.status(200).json({
          success: true,
          sattus: 200,
          message: 'A list of all puja saman',
          data: allSaman,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Something went wrong. Please try again.',
          error: err.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: 'Server error. Please try again.',
      error: err.message,
    });
  }
};

const editPujaSaman = async (req, res) => {
  try {
    const id = req.params.id;
    await pujaSamanModel.findById(id)
      .then((pujaSaman) => {
        res.status(200).json({
          success: true,
          message: `More on ${pujaSaman.title}`,
          data: pujaSaman,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'This saman does not exist',
          error: err.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: "Server error. Please try again.",
      error: error.message,
    });
  }
};

const updatePujaSaman = async (req, res) => {
  try {
    const id = req.params.id;
    const updateObject = req.body;
    var _frontImageFileName = updateObject.frontImage;
    var _backImageFileName = updateObject.backImage;
    var _thumbnailFileName = updateObject.thumbnail;
    if (req.files.frontImage && req.files.backImage && req.files.thumbnail) {
      // images is a field name
      let frontImage = req.files.frontImage;
      let backImage = req.files.backImage;
      let thumbnail = req.files.thumbnail;
      // for single image
      _frontImageFileName = frontImage.name;
      _backImageFileName = backImage.name;
      _thumbnailFileName = thumbnail.name;
      //directory to upload
      const frontImageUploadPath = path.join(__dirname, "/uploads/", frontImageFileName);
      const backImageUploadPath = path.join(__dirname, "/uploads/", backImageFileName);
      const thumbnailUploadPath = path.join(__dirname, "/uploads/", thumbnailFileName);
      frontImage.mv(frontImageUploadPath);
      backImage.mv(backImageUploadPath);
      thumbnail.mv(thumbnailUploadPath);
    }
    await pujaSamanModel
      .update({ _id: id }, { $set: { ...updateObject, frontImage: _frontImageFileName, backImage: _backImageFileName, thumbnail: _thumbnailFileName } })
      .exec()
      .then(() => {
        res.status(200).json({
          success: true,
          message: "Successfully updated",
          updatePujaSaman: updateObject,
          status: 200,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: "Something went wrong, Please try again!",
          error: err.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: "Server error. Please try again.",
      error: error.message,
    });
  }
};

const deletePujaSaman = async (req, res) => {
  try {
    const _id = req.params.id
    await pujaSamanModel
      .findByIdAndRemove(_id)
      .exec()
      .then(() =>
        res.status(200).send({
          status: 200,
          message: "Successfully deleted",
        })
      )
      .catch((err) =>
        res.status(400).json({
          success: false,
          status: 400,
          message: "Something went wrong, Please try again!",
          error: err.message,
        })
      );
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: "Server error. Please try again.",
      error: error.message,
    });
  }
};

export { createPujaSaman, getPujaSamanbyId, editPujaSaman, updatePujaSaman, deletePujaSaman };
