import headerModel from "../model/headerModel.js";
import { headerValidation } from "../utils/validator.js";

export const createHeader = async (req, res) => {
    const errMessage = headerValidation({ ...req.body });
    if (errMessage) return res.status(402).send({ status: 402, error: errMessage });
        try {
            const NewData = new headerModel({
                ...req.body
            });

            await NewData
                .save()
                .then((_data) => {
                    return res.status(200).json({
                        success: true,
                        message: 'Data created successfully',
                        data: _data,
                    });
                })
                .catch((error) => {
                    res.status(400).json({
                        success: false,
                        status: 400,
                        message: 'Something went wrong. Please try again.',
                        error: error.message,
                    });
                });
        } catch (error) {
            res.status(400).json({
                success: false,
                status: 400,
                message: 'Something went wrong. Please try again.',
                error: error.message,
            });
        }
};

export const getAllHeader = async (req, res) => {
    try {
        await headerModel.find()
            .then((_data) => {
                return res.status(200).json({
                    success: true,
                    sattus: 200,
                    message: 'Data fetched successfully.',
                    data: _data,
                });
            })
            .catch((err) => {
                res.status(400).json({
                    success: false,
                    status: 400,
                    message: 'Something went wrong. Please try again.',
                    error: err.message,
                });
            });
    } catch (error) {
        res.status(500).json({
            success: false,
            status: 500,
            message: 'Server error. Please try again.',
            error: err.message,
        });
    }
};

export const getHeaderById = async (req, res) => {
    try {
        const id = req.params.id;
        await headerModel.findById(id)
            .then((Header) => {
                res.status(200).json({
                    success: true,
                    message: `Data fetched successfully.`,
                    data: Header,
                });
            })
            .catch((err) => {
                res.status(400).json({
                    success: false,
                    status: 400,
                    message: 'Header does not exist',
                    error: err.message,
                });
            });
    } catch (error) {
        res.status(500).json({
            success: false,
            status: 500,
            message: "Server error. Please try again.",
            error: error.message,
        });
    }
};

export const updateHeader = async (req, res) => {
    try {
        const _id = req.params.id
        const updateObject = req.body;
        await headerModel
            .update({ _id }, { $set: req.body })
            .exec()
            .then(() => {
                res.status(200).json({
                    success: true,
                    message: "Successfully updated",
                    updatePujaSaman: updateObject,
                    status: 200,
                });
            })
            .catch((err) => {
                res.status(400).json({
                    success: false,
                    status: 400,
                    message: "Something went wrong, Please try again!",
                    error: err.message,
                });
            });
    } catch (error) {
        res.status(500).json({
            success: false,
            status: 500,
            message: "Server error. Please try again.",
            error: error.message,
        });
    }
};

export const deleteHeader = async (req, res) => {
    console.log(req.params.id);
    try {
        const _id = req.params.id
        await headerModel
            .findByIdAndRemove(_id)
            .exec()
            .then(() =>
                res.status(200).send({
                    status: 200,
                    message: "Successfully deleted",
                })
            )
            .catch((err) =>
                res.status(400).json({
                    success: false,
                    status: 400,
                    message: "Something went wrong, Please try again!",
                    error: err.message,
                })
            );
    } catch (error) {
        res.status(500).json({
            success: false,
            status: 500,
            message: "Server error. Please try again.",
            error: error.message,
        });
    }
};
