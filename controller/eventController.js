import path from "path";
import eventModel from "../model/eventModel.js";

const __dirname = path.resolve();


const createEvent = async (req, res) => {
  const { title, name, status, description } = req.body;
  if (!req.files) {
    res.send({
      status: 402,
      message: "Event image is required",
    });
  } else {
    let uploadedImageData = req.files.image;

    let imageName = uploadedImageData.name + new Date().getTime()

    const uploadPathImage = path.join(__dirname, "/uploads/", imageName);
    uploadedImageData.mv(uploadPathImage);

    try {
      const newContent = await eventModel.create({
        title,
        image: imageName,
        name,
        status,
        description
      })

      await newContent
      .save()
      .then((_data) => {
        return res.status(200).json({
          success: true,
          status: 200,
          message: 'Data created successfully',
          data: _data,
        });
      })
      .catch((error) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Something wnet wrong. Please try again.',
          error: error.message,
        });
      });

    } catch (error) {
      res.status(500).json({
        success: false,
        status: 500,
        message: "Server error. Please try again.",
        error: error.message,
      });
    }
  }
}
const getEventData = async (req, res) => {
  try {
    await eventModel.find()
      .then((_data) => {
        return res.status(200).json({
          success: true,
          sattus: 200,
          message: 'A list of all data.',
          data: _data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: 'Something went wrong. Please try again.',
          error: err.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      success: false,
      status: 500,
      message: 'Server error. Please try again.',
      error: err.message,
    });
  }
};

const deleteEvent = async (req, res) => {
  try {
      const _id = req.params.id
      await eventModel
          .findByIdAndRemove(_id)
          .exec()
          .then(() =>
              res.status(200).send({
                  status: 200,
                  message: "Successfully deleted",
              })
          )
          .catch((err) =>
              res.status(400).json({
                  success: false,
                  status: 400,
                  message: "Something went wrong, Please try again!",
                  error: err.message,
              })
          );
  } catch (error) {
      res.status(500).json({
          success: false,
          status: 500,
          message: "Server error. Please try again.",
          error: error.message,
      });
  }
};

const updateEvent = async (req, res) => {
    let uploadedImageData = req.files.image;

    let imageName = uploadedImageData.name + new Date().getTime()

    const uploadPathImage = path.join(__dirname, "/uploads/", imageName);
    uploadedImageData.mv(uploadPathImage);
    const id = req.params.id;
    const updateObject = req.body;
    try {
      await eventModel
      .update({ _id: id }, { $set: { ...updateObject, image: imageName } })
      .exec()
      .then(() => {
        res.status(200).json({
          success: true,
          message: "Successfully updated",
          updatePujaSaman: updateObject,
          status: 200,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          status: 400,
          message: "Something went wrong, Please try again!",
          error: err.message,
        });
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status: 500,
        message: "Server error. Please try again.",
        error: error.message,
      });
    }
  }



export {
  getEventData,
  createEvent,
  deleteEvent,
  updateEvent
};
